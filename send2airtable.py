import airtable
from lxml import etree
import urllib.request
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-t")
parser.add_argument("-l")
parser.add_argument("-s")
parser.add_argument("-e")
args = parser.parse_args()

#some change, testing VS code

if args.t == 'video':
    airtable = airtable.Airtable("appIa3SHzulgHrbrj", "Table 1")
    if "twitter.com/" in args.l:
        name = args.l.split('twitter.com/')[1].split('/')[0]
    elif "youtu" in args.l:
        youtube = etree.HTML(urllib.request.urlopen(args.l).read())
        video_title = youtube.xpath("//span[@id='eow-title']/@title")
        name = (''.join(video_title))
    else:
        name = ""

    record = {'Name': name, 'URL': args.l, 'Tag': 'video'}

if args.t == 'musique':
    if "twitter.com/" in args.l:
        name = args.l.split('twitter.com/')[1].split('/')[0]

    else:
        name = ""
    print(args.l)
    airtable = airtable.Airtable("appIa3SHzulgHrbrj", "Table 1")
    record = {'Name': name, 'URL': args.l, 'Tag': 'musique'}

if args.t == 'semra':
    # citation
    if args.s.lower() == "citation":
        airtable = airtable.Airtable("appm9o2krkeAJKpVv", "Citations")
        record = {'Auteur': 'emily', 'Citation': args.l}

    # o input
    if args.s == "noinput":
        airtable = airtable.Airtable("appm9o2krkeAJKpVv", "No Input")
        record = {'Notes': args.l}

    # extras
    if args.s == "extras":
        airtable = airtable.Airtable("appm9o2krkeAJKpVv", "Extras")
        record = {'Name': args.l}

    # surprises
    if args.s == "surprises":
        airtable = airtable.Airtable("appm9o2krkeAJKpVv", "Surprises")
        record = {'Name': args.l}

    # Benefits
    if args.s == "benefits":
        airtable = airtable.Airtable("appm9o2krkeAJKpVv", "Benefits")
        record = {'Name': args.l}


airtable.insert(record)
