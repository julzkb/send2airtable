#!/bin/bash

source app-env.sh

if [ "$1" == "video" ] | [ "$1" == "musique" ] ; then
    echo "this is a video or musique"
    python3 send2airtable.py -t "$1" -l "$2"
else
    python3 send2airtable.py -t "$1" -s "$2" -l "$3"

fi


unset AIRTABLE_API_KEY

